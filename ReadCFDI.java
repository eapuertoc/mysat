import java.util.*;
import java.io.*;
import java.nio.file.*;

public class ReadCFDI {

	public static void main(String[] args) {
		BufferedReader sr = null;
		PrintWriter pw;
		int i, n;
		double imp;
		String str, linea1, linea2, subt, desc, total, nombre, usoCFDI;
		subt = "";
		desc = "";
		total = "";
		usoCFDI = "";
		nombre = "";
		File folder = new File(".");
		String[] listOfFiles = folder.list();
		try {
			// Preparar archivo de escritura
			pw = new PrintWriter(new FileWriter("Egresos.tsv"));
			pw.println("Archivo\tNombre\tSubtotal\tDescuento\tImpuesto\tTotal\tUso CFDI");
			n = 0;
			for(i = 0; i < listOfFiles.length; i++) {
				if(listOfFiles[i].toLowerCase().contains(".xml")) {
					n++;
					System.out.println(listOfFiles[i]);
					sr = new BufferedReader(new FileReader(listOfFiles[i]));
					str = sr.readLine();
					nombre = "";
					total = "";
					usoCFDI = "";
					desc = "0.00";
					// Separa al archivo por cada vez que aparece el caracter '>' y revisa los datos de interés 
					while (str.length() != 0){
						linea1 = str.substring(0, str.indexOf('>') + 1);
						// En la línea de comprobante
						if(linea1.contains("<cfdi:Comprobante")) {
							while (linea1.contains(" ")) {
								linea2 = linea1.substring(0, linea1.indexOf(' ') + 1);
								if (linea2.contains("SubTotal")) {
									subt = linea2.substring(linea2.indexOf('"') + 1, linea2.length() - 2);
								} else if (linea2.contains("Descuento")) {
									desc = linea2.substring(linea2.indexOf('"') + 1, linea2.length() - 2);
								} else if(linea2.contains("Total")) {
									total = linea2.substring(linea2.indexOf('"') + 1, linea2.length() - 2);
								}
								linea1 = linea1.substring(linea1.indexOf(' ') + 1, linea1.length());
							}
						} else if(linea1.contains("<cfdi:Emisor")) { // En la línea de emisor
							while (linea1.contains(" ")) {
								linea2 = linea1.substring(0, linea1.indexOf(' ') + 1);
								if (linea2.contains("Nombre")) {
									nombre = linea1.substring(linea1.indexOf('"') + 1, linea1.length());
									nombre = nombre.substring(0, nombre.indexOf('"'));
								}
								linea1 = linea1.substring(linea1.indexOf(' ') + 1, linea1.length());
								}
							} else if(linea1.contains("<cfdi:Receptor")) {
								linea1 = linea1 + " ";
								while (linea1.contains(" ")) {
								    linea2 = linea1.substring(0, linea1.indexOf(' ') + 1);
								    if (linea2.contains("UsoCFDI")) {
									    usoCFDI = linea1.substring(linea1.indexOf('"') + 1, linea1.length());
									    usoCFDI = usoCFDI.substring(0, usoCFDI.indexOf('"'));
								    }
								linea1 = linea1.substring(linea1.indexOf(' ') +1, linea1.length());
								}
							}
							str = str.substring(str.indexOf('>') + 1,str.length());
							if (str.length() == 0) {
								if (nombre == "" || total == "" || usoCFDI == "")
									str = sr.readLine();
							}
					}
					imp = Double.parseDouble(total) - Double.parseDouble(subt) + Double.parseDouble(desc);
					pw.println(listOfFiles[i] + "\t" + nombre + "\t" + subt + "\t" + desc + "\t" + String.format(Locale.US, "%.2f", imp)+ "\t" + total + "\t" + usoCFDI);
				}
			}
			pw.close();
			sr.close();
			System.out.println("Adquisición de información exitosa, " + n + " archivos registrados.");
		} catch (IOException e){
			System.out.println("Error: " + e.getMessage());
		}
	}

}